import {hideLoader, showLoader} from "./loader";
import {baseURL} from "../config";
import types from "../constants/actionTypes";

return { type: types.SHOW_LOADER };

return { type: types.HIDE_LOADER };

return async dispatch => {

    dispatch(showLoader());
    const response = await fetch(`${baseURL}${lang}.json`);
    const questions = await response.json();
    dispatch({
        type: types.GET_QUESTIONS,
        questions
    });
    dispatch(hideLoader());
};

return { type: types.ADD_ANSWER, indexCheckbox, value, indexQuestion };

return { type: types.PUSH_ROUTE, routeName };

return { type: types.GET_BACK };

return { type: types.CLEAR_HISTORY };

return { type: types.SET_THEME, theme };

return { type: types.SET_USER_EMAIL, email };

