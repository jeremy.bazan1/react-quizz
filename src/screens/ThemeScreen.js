import React, { Component } from "react";
import LangSelect from "../components/LangSelect";
import GoBackButton from "../containers/GoBackButton";

const languages = [
  {
    id: 0,
    source: "./html-logo.png",
    alt: "html",
    label: "HTML",
    path: "html"
  },
  {
    id: 1,
    source: "./js-logo.png",
    alt: "javascript",
    label: "Javascript",
    path: "js"
  },
  {
    id: 2,
    source: "./php-logo.png",
    alt: "php",
    label: "PHP",
    path: "php"
  }
];

export default class ThemeScreen extends Component {
  handleSelect = selected => {
    this.props.actions.setTheme(selected);
    this.props.actions.pushRoute("questions");
  };

  render() {
    return (
      <section className="theme-wrapper">
        <div>
          <GoBackButton />
        </div>
        <div className="theme-select">
          {languages.map(lang => {
            return (
              <LangSelect
                key={lang.id}
                source={lang.source}
                alt={lang.alt}
                label={lang.label}
                onClick={this.handleSelect}
                path={lang.path}
              />
            );
          })}
        </div>
      </section>
    );
  }
}
