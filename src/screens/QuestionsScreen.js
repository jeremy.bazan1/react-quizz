import React, { Component, Fragment } from "react";
import Button from "../components/Button";
import Question from "../containers/Question";
import Loader from "../components/Loader";
import GoBackButton from "../containers/GoBackButton";

export default class QuestionsScreen extends Component {
  componentDidMount() {
    const { theme, actions } = this.props;
    /**
     * Fetch data through redux action
     */
    actions.getQuestionsByLang(theme);
  }

  handleSubmit = event => {
    // Prevent browser from reloading
    event.preventDefault();
    console.log("here");
    const { actions } = this.props;
    actions.pushRoute("results");
  };

  render() {
    const { theme, questions, isLoading } = this.props;
    return (
      <Fragment>
        <section className="questions-wrapper">
          <div style={{ marginBottom: 50 }}>
            <h2 style={{ textAlign: "center", fontSize: 24 }}>
              Vous avez choisi le thème {theme}
            </h2>
            <GoBackButton />
          </div>
          {isLoading ? (
            <Loader />
          ) : (
            questions.map((question, index) => {
              return (
                <Question
                  key={question.index}
                  question={question.question}
                  answers={question.answers}
                  index={index}
                />
              );
            })
          )}
          <div style={{ marginTop: 50 }}>
            <Button
              onClick={this.handleSubmit}
              label="Valider le questionnaire"
            />
          </div>
        </section>
      </Fragment>
    );
  }
}
