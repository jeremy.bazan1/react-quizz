import React, { Component } from "react";
import InputField from "../components/InputField";
import Button from "../components/Button";
import { validateEmail } from "../helpers/validation";

export default class HomeScreen extends Component {
  state = {
    email: "",
    error: {}
  };

  handleChange = event => {
    const value = event.target.value;
    this.setState({ email: value });
  };

  handleSubmit = event => {
    event.preventDefault();

    const { email } = this.state;
    if (!validateEmail(email)) {
      this.setState({ error: { message: "Entrez une adresse email valide" } });
    } else {
      this.setState({ error: {} });
      this.props.actions.setUserEmail(email);
      this.props.actions.pushRoute("theme");
    }
  };

  render() {
    const { error } = this.state;
    return (
      <section className="home-wrapper">
        <h2>
          Pour participer au quiz, entrez votre adresse email ci-dessous :
        </h2>
        <form onSubmit={this.handleSubmit}>
          <InputField
            name="email"
            label="Votre email"
            onChange={this.handleChange}
          />
          <Button label="Continuer" onClick={this.handleSubmit} />
          {error.message && <span className="error">{error.message}</span>}
        </form>
      </section>
    );
  }
}
