import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import HomeScreen from "../screens/HomeScreen";
import { setUserEmail } from "../actions/user";
import { pushRoute } from "../actions/router";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return { email: state.user.email };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ setUserEmail, pushRoute }, dispatch)
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);
