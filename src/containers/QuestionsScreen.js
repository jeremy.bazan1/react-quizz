import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getQuestionsByLang } from "../actions/question";
import { getBack, pushRoute } from "../actions/router";
import QuestionScreen from "../screens/QuestionsScreen";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return {
    theme: state.theme.selectedTheme,
    isLoading: state.loader.isLoading,
    questions: state.question.questions
  };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      { pushRoute, getQuestionsByLang, getBack },
      dispatch
    )
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionScreen);
