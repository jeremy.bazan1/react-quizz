import { combineReducers } from "redux";
import { router } from "./router";
import { user } from "./user";
import { theme } from "./theme";
import { loader } from "./loader";
import { question } from "./question";

export default combineReducers({ router, user, theme, question, loader });
