import types from "../constants/actionTypes";


export const user = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER_EMAIL:
      return {
        email: action.email
      };
    default:
      return state;
  }
};
