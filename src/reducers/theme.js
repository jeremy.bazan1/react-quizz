import types from "../constants/actionTypes";


export const theme = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_THEME:
      return { selectedTheme: action.theme };
    default:
      return state;
  }
};
