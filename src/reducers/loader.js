import types from "../constants/actionTypes";

export const loader = (state = initialState, action) => {
  switch (action.type) {
    case types.SHOW_LOADER:

    case types.HIDE_LOADER:

    default:
      return state;
  }
};
