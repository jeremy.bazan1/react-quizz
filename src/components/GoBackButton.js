import React, { Component } from "react";
import Button from "../components/Button";

export default class GoBackButton extends Component {
  render() {
    const { actions } = this.props;
    return (
      <Button
        isGoBack
        label="Revenir en arrière"
        onClick={() => actions.getBack()}
      />
    );
  }
}
