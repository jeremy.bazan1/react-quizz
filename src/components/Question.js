import React, { Component } from "react";
import Answer from "./Answer";

export default class Question extends Component {


  handleChange = (indexCheckbox, e) => {
    const { actions, index } = this.props;
    const value = e.target.checked;
    actions.addAnswer(indexCheckbox, value, index);
  };

  render() {
    const { question, answers } = this.props;
    return (
      <div>
        <h5>{question}</h5>
        <form>
          {answers.map((answer, index) => (
            <Answer
              key={index}
              onChange={this.handleChange}
              index={index}
              answer={answer}
            />
          ))}
        </form>
      </div>
    );
  }
}
