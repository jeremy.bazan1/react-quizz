import React, { Component } from "react";
import Button from "./Button";

export default class LangSelect extends Component {
  render() {
    const { source, alt, label, onClick, path } = this.props;
    return (
      <div className="lang-wrapper">
        <img className="lang-logo" src={source} alt={alt} />
        <Button label={label} onClick={() => onClick(path)} />
      </div>
    );
  }
}
