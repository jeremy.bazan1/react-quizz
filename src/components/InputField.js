import React, { Component } from "react";

export default class InputField extends Component {
  render() {
    const { name, label, onChange } = this.props;
    return (
      <div className="input-field">
        <label htmlFor={name}>{label}</label>
        <input name={name} onChange={event => onChange(event)} type="text" />
      </div>
    );
  }
}
