import React, { Component } from "react";

export default class Answer extends Component {

  onChange = (e) => {
    const { onChange, index } = this.props;
    onChange(index, e);
  }

  render() {
    const { answer } = this.props;
    return (
      <div>
        <label htmlFor={answer.answer}>
          <input
            type="checkbox"
            onChange={this.onChange}
            id={answer.answer}
          />
          <span>{answer.answer}</span>
        </label>
      </div>
    );
  }
}
