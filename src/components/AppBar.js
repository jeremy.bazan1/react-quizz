import React, { Component } from "react";

export default class AppBar extends Component {

  render() {
    return (
        <nav>
          <div className="nav-wrapper">
            <span className="brand-logo center">Web Quizz</span>
          </div>
        </nav>
    );
  }
}