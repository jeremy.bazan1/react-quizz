import React, { Component } from "react";

export default class Button extends Component {
  render() {
    const { label, onClick, isGoBack } = this.props;
    return (
      <button
        className={
          isGoBack
            ? "gobackbutton-color waves-effect waves-light btn"
            : "waves-effect waves-light btn"
        }
        onClick={onClick}
      >
        {label}
      </button>
    );
  }
}
