import React from "react";
import HomeScreen from "../containers/HomeScreen";
import ThemeScreen from "../containers/ThemeScreen";
import QuestionsScreen from "../containers/QuestionsScreen";
import ResultsScreen from "../containers/ResultsScreen";

export const renderCurrentRoute = routeName => {
  switch (routeName) {
    case "home":
      return <HomeScreen />;
    case "theme":
      return <ThemeScreen />;
    case "questions":
      return <QuestionsScreen />;
    case "results":
      return <ResultsScreen />;
    default:
      return <HomeScreen />;
  }
};
